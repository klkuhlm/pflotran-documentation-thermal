References
==========

Balay, S., V. Eijkhout V, W.D. Gropp, L.C. McInnes and B.F. Smith (1997)
Modern Software Tools in Scientific Computing, Eds. Arge E, Bruaset AM
and Langtangen HP (Birkhaüser Press), pp. 163–202.

Chen, J., J. W. Hopmans, and M. E. Grismer (1999) Parameter estimation of 
two-fluid capillary pressure-saturation and permeability functions, Advances in
Water Resources, Vol. 22, No. 5, pp. 479-493.

Coats, K.H. and A.B. Ramesh (1982) Effects of Grid Type and Difference
Scheme on Pattern Steamflood Simulation Results, paper SPE-11079,
presented at the 57th Annual Fall Technical Conference and Exhibition of
the Society of Petroleum Engineers, New Orleans, LA, September 1982.

Ebigbo, A., Holger Class, H., Helmig, R. (2007) :math:`\mathrm{CO_2}`
leakage through an abandoned well: problem-oriented benchmarks, Computers in
Geosciences 11:103?115 DOI 10.1007/s10596-006-9033-7.

Fenghour, A., W.A. Wakeham, and V. Vesovic (1998) The viscosity of
carbon dioxide, J. Phys. Chem. Ref. Data, 27(1), 31–44.

Goode, D.J. (1996) Direct simulation of groundwater age, Water Resources
Research, 32, 289–296.

Hammond, G.E., P.C. Lichtner, C. Lu, and R.T. Mills (2011) PFLOTRAN:
Reactive Flow & Transport Code for Use on Laptops to Leadership-Class
Supercomputers, Editors: Zhang, F., G. T. Yeh, and J. C. Parker, *Ground
Water Reactive Transport Models*, Bentham Science Publishers. ISBN
978-1-60805-029-1.

Khaleel, R., E.J. Freeman (1995) Variability and scaling of hydraulic
properties for 200 area soils, Hanford Site. Report WHC-EP-0883.
Westinghouse Hanford Company, Richland, WA.

Khaleel, R., T.E. Jones, A.J. Knepp, F.M. Mann, D.A. Myers, P.M. Rogers,
R.J. Serne, and M.I. Wood (2000) Modeling data package for S-SX Field
Investigation Report (FIR). Report RPP-6296, Rev. 0. CH2M Hill Hanford
Group, Richland, WA.

Lichtner, P.C., Yabusaki, S.B., Pruess K., and Steefel, C.I. (2004) Role
of Competitive Cation Exchange on Chromatographic Displacement of Cesium
in the Vadose Zone Beneath the Hanford S/SX Tank Farm, *VJZ*, **3**,
203–219.

Lichtner, P.C. (1996a) Continuum Formulation of
Multicomponent-Multiphase Reactive Transport, In: *Reactive Transport in
Porous Media* (eds. P. C. Lichtner, C. I. Steefel, and E. H. Oelkers),
*Reviews in Mineralogy*, **34**, 1–81.

Lichtner P.C. (1996b) Modeling Reactive Flow and Transport in Natural
Systems, Proceedings of the Rome Seminar on Environmental Geochemistry,
Eds. G. Ottonello and L. Marini, Castelnuovo di Porto, May 22–26, Pacini
Editore, Pisa, Italy, 5–72.

Lichtner, P.C. (2000) Critique of Dual Continuum Formulations of
Multicomponent Reactive Transport in Fractured Porous Media, Ed. Boris
Faybishenko, *Dynamics of Fluids in Fractured Rock*, Geophysical
Monograph **122**, 281–298.

Lichtner, P.C. and Karra, S. (2014) Modeling multiscale-multiphase-multicomponent reactive flows in porous media: Application to :math:`\mathrm{CO_2}` sequestration and enhanced geothermal energy using PFLOTRAN, In: *Computational Models for* :math:`\mathrm{CO_2}` *Geo-Sequestration \& Compressed Air Energy Storage*, Editors: Rafid Al-Khoury, Jochen Bundschuh, CRC Press, p. 81–136.

Lichtner, P.C. (2016) Kinetic rate laws invariant to scaling the mineral formula unit,
American Journal of Science, **316**, 437–469.

Painter, S.L. (2011) Three-phase numerical model of water migration in
partially frozen geological media: model formulation, validation, and
applications, Computational Geosciences **15**, 69-85.

Peaceman, D.W. (1977) Interpretation of Well-Block Pressures in
Numerical Reservoir Simulation with Nonsquare Grid Blocks and
Anisotropic Permeability, paper SPE-10528, presented at the Sixth SPE
Symposium on Reservoir Simulation of the Society of Petroleum Engineers,
New Orleans, LA, January 1982.

Pruess, K., S. Yabusaki, C. Steefel, and P. Lichtner (2002) Fluid flow,
heat transfer, and solute transport at nuclear waste storage tanks in
the Hanford vadose zone. Available at www.vadosezonejournal.org. Vadose
Zone J. **1**, 68–88.

Pruess, K., and Narasimhan (1985) A practical method for modeling fluid
and heat flow in fractured porous media, SPE 10509, 14–26.

Somerton, W.H., A.H. El-Shaarani, and S.M. Mobarak (1974) High
temperature behavior of rocks associated with geothermal-type
reservoirs. Paper SPE-4897. Proceedings of the 44th Annual California
Regional Meeting of the Society of Petroleum Engineers. Richardson, TX:
Society of Petroleum Engineers.

Andreas Voegelin, Vijay M. Vulava, Florian Kuhnen, Ruben Kretzschmar
(2000) Multicomponent transport of major cations predicted from binary
adsorption experiments, Journal of Contaminant Hydrology, **46**, 319–338.
