#!/bin/bash

echo 'beginning deployment'

echo 'setting up ssh'
echo -e $PRIVATE_SSH_KEY >> /root/.ssh/id_rsa
chmod 600 /root/.ssh/id_rsa
echo 'transfering tarball'
scp -P 2222 -oStrictHostKeyChecking=no /tmp/codeship.tar.gz pflotran@108.167.189.107:~
exit_status=$?
if [ $exit_status -eq 0 ]; then
  echo 'transfer successful'
  echo 'extracting tarball to public_html/documentation/.'
  ssh -p 2222 -oStrictHostKeyChecking=no pflotran@108.167.189.107 "/bin/rm -Rf public_html/documentation/* && tar -xzvf codeship.tar.gz -C public_html/documentation/. && /bin/rm codeship.tar.gz"
  exit_status=$?
  if [ $exit_status -eq 0 ]; then
    echo 'extraction successful'
    echo 'successful deployment'
  else
    echo 'extraction failed'
  fi
else
  echo 'transfer failed'
fi
exit $exit_status
